# cluster-job-submission

1. Connect to FortiClient
    Make sure that you are on OzU network.
2. Use the following command to connect to the machine:
    $ ssh -X username@headnode.hpc.ozyegin.edu.tr

    alternative:
    $ ssh -X username@10.111.1.99

3. Copy your source file(s) to Cluster:
    $ scp sourcefiles username@headnode.hpc.ozyegin.edu.tr:~/
    This command will copy source files to user's remote home directory.

4. Use mpicc to compile the code:
    $ mpicc -g -Wall -o objectname sourcename

5. Test the execution:
   $ mpirun -np 2 ./objectname

6. Modify the batch script:

Shebang which tells the name of the shell interpreter
#!/bin/bash
#

Name of the queue, We will be using gpuq for the course.
#SBATCH -p gpuq   # Name of the queue
##SBATCH -A abasol

Job name of your preference
#SBATCH --job-name=mpi_c

Time limit in Minutes
#SBATCH --time=100

Standard output is written to this file (%j is your job ID)
#SBATCH --output=slurm-%j.out

Standard error is written to this file (%j is your job ID)
#SBATCH --error=slurm-%j.err

Number of nodes
#SBATCH -N 2

Number of tasks per node
#SBATCH --ntasks-per-node=4
##SBATCH --mem-per-cpu=80000

##SBATCH --mem=240000

ulimit -s 10240
You give the executable here:
mpirun ./trap

cat $HOSTFILE
rm -f $HOSTFILE

To submit a job:
$ sbatch batch_mpi.sh

To see the queue:
$ squeue

