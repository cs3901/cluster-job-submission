#!/bin/bash
#

#SBATCH -p gpuq   # kuyruk ismi
##SBATCH -A abasol

#SBATCH --job-name=mpi_c

#SBATCH --time=100
#SBATCH --output=slurm-%j.out
#SBATCH --error=slurm-%j.err

#SBATCH -N 2
#SBATCH --ntasks-per-node=4
##SBATCH --mem-per-cpu=80000

##SBATCH --mem=240000

ulimit -s 10240
mpirun ./trap
cat $HOSTFILE
rm -f $HOSTFILE
