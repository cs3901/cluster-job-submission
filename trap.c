#include <stdio.h>
#include <mpi.h>

double f(double);

int main(void) {
    int my_rank;
    int comm_size;
    int n = 128; /* global number of trapezoids.*/
    int i;
    int source;
    int local_n;
    double a = 1.0;
    double b = 5.0;
    double local_a; /*different for each process.*/
    double local_b; /*different for each process.*/
    double h = (b -a) / n;
    double local_area = 0.;
    double total_area = 0.;


    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    /* Calculate local values */
    local_n = n / comm_size;
    local_a = a + my_rank*local_n*h;
    local_b = local_a + local_n*h;

    for(i=0;i<local_n;++i) {
        local_area += h*(f(local_a+i*h) + f(local_a+(i+1)*h))/2;
    }
    printf("My rank is %d.\n My local area is %f\n", my_rank, local_area);

    /* A */
    /* Send local area to master */
    if(my_rank != 0) {
        MPI_Send(&local_area, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    } else {
        total_area += local_area;
        for (source=1;source<comm_size;++source) {
            MPI_Recv(&local_area, 1, MPI_DOUBLE, source, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            printf("Received area as %f from process %d\n", local_area, source);
            total_area += local_area;
        }
   }

   if (my_rank == 0) {
       printf("My total area is %f\n", total_area);
   }

    MPI_Finalize();
    return 0;
}

double f(double x) {
    return x;
}

